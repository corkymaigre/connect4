﻿namespace Connect4.Model
{

    class Game
    {
        public const int rowMax = 6;
        public const int colMax = 7;

        private bool isHorizontal = false;
        private bool isVertical = false;
        private bool isSlash = false;
        private bool isBackSlash = false;

        private int winnerPlayer;
        private int activePlayer = 1;
        private int[,] tab;

        public int ActivePlayer
        {
            get
            {
                return activePlayer;
            }

            set
            {
                activePlayer = value;
            }
        }

        public int[,] Tab
        {
            get
            {
                return tab;
            }

            set
            {
                tab = value;
            }
        }

        public bool IsHorizontal
        {
            get
            {
                return isHorizontal;
            }

            set
            {
                isHorizontal = value;
            }
        }

        public bool IsVertical
        {
            get
            {
                return isVertical;
            }

            set
            {
                isVertical = value;
            }
        }

        public bool IsSlash
        {
            get
            {
                return isSlash;
            }

            set
            {
                isSlash = value;
            }
        }

        public bool IsBackSlash
        {
            get
            {
                return isBackSlash;
            }

            set
            {
                isBackSlash = value;
            }
        }

        public int WinnerPlayer
        {
            get
            {
                return winnerPlayer;
            }

            set
            {
                winnerPlayer = value;
            }
        }

        public Game()
        {
            Tab = new int[rowMax, colMax];
            for (int i = 0; i < rowMax; i++)
            {
                for (int j = 0; j < colMax; j++)
                {
                    Tab[i, j] = 0;
                }
            }
            activePlayer = 1;
        }

        private int ColumnStringToInt(string str)
        {
            int j = 0;
            switch (str)
            {
                case "A":
                    j = 0;
                    break;
                case "B":
                    j = 1;
                    break;
                case "C":
                    j = 2;
                    break;
                case "D":
                    j = 3;
                    break;
                case "E":
                    j = 4;
                    break;
                case "F":
                    j = 5;
                    break;
                case "G":
                    j = 6;
                    break;
            }
            return j;
        }

        private void ChangePlayer()
        {
            if (activePlayer == 1)
                activePlayer = 2;
            else
                activePlayer = 1;
        }

        public int AddToken(string col)
        {
            int player;
            int i = 0;
            int j = ColumnStringToInt(col);
            do
            {
                if (Tab[i, j] == 0)
                {
                    Tab[i, j] = ActivePlayer;
                    break;
                }
                else
                    i++;
            }
            while (i < rowMax);
            CheckWinner();
            player = activePlayer;
            ChangePlayer();
            return player;         
        }

        public int GetValueOf(int i, int j)
        {
            return Tab[i, j];
        } 

        private void CheckWinner()
        {
            HorizontalCheck();
            VerticalCheck();
            SlashCheck();
            BackSlashCheck();
        }

        private void HorizontalCheck()
        {
            for (int i = 0; i < rowMax; i++)
            {
                for (int j = 0; j < colMax / 2 + 1; j++)
                {
                    if (Tab[i, j] == Tab[i, j+1] && Tab[i, j] == Tab[i, j+2] && Tab[i, j] == Tab[i, j+3])
                    {
                        if (Tab[i, j] != 0)
                        {
                            WinnerPlayer = Tab[i, j];
                            Tab[i, j] = 99;
                            Tab[i, j + 1] = 99;
                            Tab[i, j + 2] = 99;
                            Tab[i, j + 3] = 99;
                        }
                    }                        
                }
            }
        }

        private void VerticalCheck()
        {
            for (int i = 0; i < rowMax / 2; i++)
            {
                for (int j = 0; j < colMax; j++)
                {
                    if (Tab[i, j] == Tab[i + 1, j] && Tab[i, j] == Tab[i + 2, j] && Tab[i, j] == Tab[i + 3, j])
                    {
                        if (Tab[i, j] != 0)
                        {
                            WinnerPlayer = Tab[i, j];
                            Tab[i, j] = 99;
                            Tab[i + 1, j] = 99;
                            Tab[i + 2, j] = 99;
                            Tab[i + 3, j] = 99;
                        }
                    }
                }
            }
        }

        private void SlashCheck()
        {
            for (int i = 0; i < rowMax / 2; i++)
            {
                for (int j = 0; j < colMax / 2 + 1; j++)
                {
                    if (Tab[i, j] == Tab[i+1, j+1] && Tab[i, j] == Tab[i+2, j+2] && Tab[i, j] == Tab[i+3, j+3])
                    {
                        if (Tab[i, j] != 0)
                        {
                            WinnerPlayer = Tab[i, j];
                            Tab[i, j] = 99;
                            Tab[i + 1, j + 1] = 99;
                            Tab[i + 2, j + 2] = 99;
                            Tab[i + 3, j + 3] = 99;
                        }
                    }
                }
            }
        }

        private void BackSlashCheck()
        {
            for (int i = 0; i < rowMax / 2; i++)
            {
                for (int j = colMax - 1; j > colMax / 3; j--)
                {
                    if (Tab[i, j] == Tab[i + 1, j - 1] && Tab[i, j] == Tab[i + 2, j - 2] && Tab[i, j] == Tab[i + 3, j - 3])
                    {
                        if (Tab[i, j] != 0)
                        {
                            WinnerPlayer = Tab[i, j];
                            Tab[i, j] = 99;
                            Tab[i + 1, j - 1] = 99;
                            Tab[i + 2, j - 2] = 99;
                            Tab[i + 3, j - 3] = 99;
                        }
                    }
                }
            }
        }
    }
}

