﻿using Connect4.View;
using Connect4.ViewModel;
using System.Windows;

namespace Connect4
{
    /// <summary>
    /// Logique d'interaction pour App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            new MainWindow()
            {
                DataContext = new MainWindowViewModel()
            }.Show();
        }
    }
}
