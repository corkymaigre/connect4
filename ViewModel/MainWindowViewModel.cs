﻿using Connect4.Model;
using System.Windows;
using System.Windows.Input;

namespace Connect4.ViewModel
{
    class MainWindowViewModel : ViewModelBase
    {
        const string token0 = "/Connect4;component/Images/0.png";
        const string token1 = "/Connect4;component/Images/1.png";
        const string token2 = "/Connect4;component/Images/2.png";
        const string token99 = "/Connect4;component/Images/99.png";

        Game myGame;

        RelayCommand<object> _newGameCommand;
        RelayCommand<string> _playCommand;
        RelayCommand<object> _aboutCommand;
        RelayCommand<object> _helpCommand;

        public MainWindowViewModel()
        {
        }

        private string _contentVisibility = "Hidden";
        public string ContentVisibility
        {
            get { return _contentVisibility; }
            set
            {
                if (value != _contentVisibility)
                {
                    _contentVisibility = value;
                    NotifyPropertyChanged("ContentVisibility");
                }
            }
        }

        private string _a0 = token0;
        public string A0
        {
            get { return _a0; }
            set
            {
                if (value != _a0)
                {
                    _a0 = value;
                    NotifyPropertyChanged("A0");
                }
            }
        }

        private string _a1 = token0;
        public string A1
        {
            get { return _a1; }
            set
            {
                if (value != _a1)
                {
                    _a1 = value;
                    NotifyPropertyChanged("A1");
                }
            }
        }

        private string _a2 = token0;
        public string A2
        {
            get { return _a2; }
            set
            {
                if (value != _a2)
                {
                    _a2 = value;
                    NotifyPropertyChanged("A2");
                }
            }
        }

        private string _a3 = token0;
        public string A3
        {
            get { return _a3; }
            set
            {
                if (value != _a3)
                {
                    _a3 = value;
                    NotifyPropertyChanged("A3");
                }
            }
        }

        private string _a4 = token0;
        public string A4
        {
            get { return _a4; }
            set
            {
                if (value != _a4)
                {
                    _a4 = value;
                    NotifyPropertyChanged("A4");
                }
            }
        }

        private string _a5 = token0;
        public string A5
        {
            get { return _a5; }
            set
            {
                if (value != _a5)
                {
                    _a5 = value;
                    NotifyPropertyChanged("A5");
                }
            }
        }

        private string _b0 = token0;
        public string B0
        {
            get { return _b0; }
            set
            {
                if (value != _b0)
                {
                    _b0 = value;
                    NotifyPropertyChanged("B0");
                }
            }
        }

        private string _b1 = token0;
        public string B1
        {
            get { return _b1; }
            set
            {
                if (value != _b1)
                {
                    _b1 = value;
                    NotifyPropertyChanged("B1");
                }
            }
        }

        private string _b2 = token0;
        public string B2
        {
            get { return _b2; }
            set
            {
                if (value != _b2)
                {
                    _b2 = value;
                    NotifyPropertyChanged("B2");
                }
            }
        }

        private string _b3 = token0;
        public string B3
        {
            get { return _b3; }
            set
            {
                if (value != _b3)
                {
                    _b3 = value;
                    NotifyPropertyChanged("B3");
                }
            }
        }

        private string _b4 = token0;
        public string B4
        {
            get { return _b4; }
            set
            {
                if (value != _b4)
                {
                    _b4 = value;
                    NotifyPropertyChanged("B4");
                }
            }
        }

        private string _b5 = token0;
        public string B5
        {
            get { return _b5; }
            set
            {
                if (value != _b5)
                {
                    _b5 = value;
                    NotifyPropertyChanged("B5");
                }
            }
        }

        private string _c0 = token0;
        public string C0
        {
            get { return _c0; }
            set
            {
                if (value != _c0)
                {
                    _c0 = value;
                    NotifyPropertyChanged("C0");
                }
            }
        }

        private string _c1 = token0;
        public string C1
        {
            get { return _c1; }
            set
            {
                if (value != _c1)
                {
                    _c1 = value;
                    NotifyPropertyChanged("C1");
                }
            }
        }

        private string _c2 = token0;
        public string C2
        {
            get { return _c2; }
            set
            {
                if (value != _c2)
                {
                    _c2 = value;
                    NotifyPropertyChanged("C2");
                }
            }
        }

        private string _c3 = token0;
        public string C3
        {
            get { return _c3; }
            set
            {
                if (value != _c3)
                {
                    _c3 = value;
                    NotifyPropertyChanged("C3");
                }
            }
        }

        private string _c4 = token0;
        public string C4
        {
            get { return _c4; }
            set
            {
                if (value != _c4)
                {
                    _c4 = value;
                    NotifyPropertyChanged("C4");
                }
            }
        }

        private string _c5 = token0;
        public string C5
        {
            get { return _c5; }
            set
            {
                if (value != _c5)
                {
                    _c5 = value;
                    NotifyPropertyChanged("C5");
                }
            }
        }

        private string _d0 = token0;
        public string D0
        {
            get { return _d0; }
            set
            {
                if (value != _d0)
                {
                    _d0 = value;
                    NotifyPropertyChanged("D0");
                }
            }
        }

        private string _d1 = token0;
        public string D1
        {
            get { return _d1; }
            set
            {
                if (value != _d1)
                {
                    _d1 = value;
                    NotifyPropertyChanged("D1");
                }
            }
        }

        private string _d2 = token0;
        public string D2
        {
            get { return _d2; }
            set
            {
                if (value != _d2)
                {
                    _d2 = value;
                    NotifyPropertyChanged("D2");
                }
            }
        }

        private string _d3 = token0;
        public string D3
        {
            get { return _d3; }
            set
            {
                if (value != _d3)
                {
                    _d3 = value;
                    NotifyPropertyChanged("D3");
                }
            }
        }

        private string _d4 = token0;
        public string D4
        {
            get { return _d4; }
            set
            {
                if (value != _d4)
                {
                    _d4 = value;
                    NotifyPropertyChanged("D4");
                }
            }
        }

        private string _d5 = token0;
        public string D5
        {
            get { return _d5; }
            set
            {
                if (value != _d5)
                {
                    _d5 = value;
                    NotifyPropertyChanged("D5");
                }
            }
        }

        private string _e0 = token0;
        public string E0
        {
            get { return _e0; }
            set
            {
                if (value != _e0)
                {
                    _e0 = value;
                    NotifyPropertyChanged("E0");
                }
            }
        }

        private string _e1 = token0;
        public string E1
        {
            get { return _e1; }
            set
            {
                if (value != _e1)
                {
                    _e1 = value;
                    NotifyPropertyChanged("E1");
                }
            }
        }

        private string _e2 = token0;
        public string E2
        {
            get { return _e2; }
            set
            {
                if (value != _e2)
                {
                    _e2 = value;
                    NotifyPropertyChanged("E2");
                }
            }
        }

        private string _e3 = token0;
        public string E3
        {
            get { return _e3; }
            set
            {
                if (value != _e3)
                {
                    _e3 = value;
                    NotifyPropertyChanged("E3");
                }
            }
        }

        private string _e4 = token0;
        public string E4
        {
            get { return _e4; }
            set
            {
                if (value != _e4)
                {
                    _e4 = value;
                    NotifyPropertyChanged("E4");
                }
            }
        }

        private string _e5 = token0;
        public string E5
        {
            get { return _e5; }
            set
            {
                if (value != _e5)
                {
                    _e5 = value;
                    NotifyPropertyChanged("E5");
                }
            }
        }

        private string _f0 = token0;
        public string F0
        {
            get { return _f0; }
            set
            {
                if (value != _f0)
                {
                    _f0 = value;
                    NotifyPropertyChanged("F0");
                }
            }
        }

        private string _f1 = token0;
        public string F1
        {
            get { return _f1; }
            set
            {
                if (value != _f1)
                {
                    _f1 = value;
                    NotifyPropertyChanged("F1");
                }
            }
        }

        private string _f2 = token0;
        public string F2
        {
            get { return _f2; }
            set
            {
                if (value != _f2)
                {
                    _f2 = value;
                    NotifyPropertyChanged("F2");
                }
            }
        }

        private string _f3 = token0;
        public string F3
        {
            get { return _f3; }
            set
            {
                if (value != _f3)
                {
                    _f3 = value;
                    NotifyPropertyChanged("F3");
                }
            }
        }

        private string _f4 = token0;
        public string F4
        {
            get { return _f4; }
            set
            {
                if (value != _f4)
                {
                    _f4 = value;
                    NotifyPropertyChanged("F4");
                }
            }
        }

        private string _f5 = token0;
        public string F5
        {
            get { return _f5; }
            set
            {
                if (value != _f5)
                {
                    _f5 = value;
                    NotifyPropertyChanged("F5");
                }
            }
        }

        private string _g0 = token0;
        public string G0
        {
            get { return _g0; }
            set
            {
                if (value != _g0)
                {
                    _g0 = value;
                    NotifyPropertyChanged("G0");
                }
            }
        }

        private string _g1 = token0;
        public string G1
        {
            get { return _g1; }
            set
            {
                if (value != _g1)
                {
                    _g1 = value;
                    NotifyPropertyChanged("G1");
                }
            }
        }

        private string _g2 = token0;
        public string G2
        {
            get { return _g2; }
            set
            {
                if (value != _g2)
                {
                    _g2 = value;
                    NotifyPropertyChanged("G2");
                }
            }
        }

        private string _g3 = token0;
        public string G3
        {
            get { return _g3; }
            set
            {
                if (value != _g3)
                {
                    _g3 = value;
                    NotifyPropertyChanged("G3");
                }
            }
        }

        private string _g4 = token0;
        public string G4
        {
            get { return _g4; }
            set
            {
                if (value != _g4)
                {
                    _g4 = value;
                    NotifyPropertyChanged("G4");
                }
            }
        }

        private string _g5 = token0;
        public string G5
        {
            get { return _g5; }
            set
            {
                if (value != _g5)
                {
                    _g5 = value;
                    NotifyPropertyChanged("G5");
                }
            }
        }


        public ICommand NewGameCommand
        {
            get
            {
                if (_newGameCommand == null)
                {
                    _newGameCommand = new RelayCommand<object>(param => this.NewGameCommandExecute(), param => this.NewGameCommandCanExecute);
                }
                return _newGameCommand;
            }
        }

        void NewGameCommandExecute()
        {
            myGame = new Game();
            Display();
            ContentVisibility = "Visible";            
        }

        bool NewGameCommandCanExecute
        {
            get { return true; }
        }


        public ICommand PlayCommand
        {
            get
            {
                if (_playCommand == null)
                {
                    _playCommand = new RelayCommand<string>(param => this.PlayCommandExecute(param), param => this.PlayCommandCanExecute);
                }
                return _playCommand;
            }
        }

        void PlayCommandExecute(string param)
        {
            int player = myGame.AddToken(param);
            Display();
            Ending();
            Display();
        }

        bool PlayCommandCanExecute
        {
            get { return true; }
        }

        private string ImageMatching(int value)
        {
            switch(value)
            {
                case 0:
                    return token0;
                case 1:
                    return token1;
                case 2:
                    return token2;
                case 99:
                    return token99;
                default:
                    return token0;
            }
        }
        private void Display()
        {
            A0 = ImageMatching(myGame.GetValueOf(0, 0));
            A1 = ImageMatching(myGame.GetValueOf(1, 0));
            A2 = ImageMatching(myGame.GetValueOf(2, 0));
            A3 = ImageMatching(myGame.GetValueOf(3, 0));
            A4 = ImageMatching(myGame.GetValueOf(4, 0));
            A5 = ImageMatching(myGame.GetValueOf(5, 0));

            B0 = ImageMatching(myGame.GetValueOf(0, 1));
            B1 = ImageMatching(myGame.GetValueOf(1, 1));
            B2 = ImageMatching(myGame.GetValueOf(2, 1));
            B3 = ImageMatching(myGame.GetValueOf(3, 1));
            B4 = ImageMatching(myGame.GetValueOf(4, 1));
            B5 = ImageMatching(myGame.GetValueOf(5, 1));

            C0 = ImageMatching(myGame.GetValueOf(0, 2));
            C1 = ImageMatching(myGame.GetValueOf(1, 2));
            C2 = ImageMatching(myGame.GetValueOf(2, 2));
            C3 = ImageMatching(myGame.GetValueOf(3, 2));
            C4 = ImageMatching(myGame.GetValueOf(4, 2));
            C5 = ImageMatching(myGame.GetValueOf(5, 2));

            D0 = ImageMatching(myGame.GetValueOf(0, 3));
            D1 = ImageMatching(myGame.GetValueOf(1, 3));
            D2 = ImageMatching(myGame.GetValueOf(2, 3));
            D3 = ImageMatching(myGame.GetValueOf(3, 3));
            D4 = ImageMatching(myGame.GetValueOf(4, 3));
            D5 = ImageMatching(myGame.GetValueOf(5, 3));

            E0 = ImageMatching(myGame.GetValueOf(0, 4));
            E1 = ImageMatching(myGame.GetValueOf(1, 4));
            E2 = ImageMatching(myGame.GetValueOf(2, 4));
            E3 = ImageMatching(myGame.GetValueOf(3, 4));
            E4 = ImageMatching(myGame.GetValueOf(4, 4));
            E5 = ImageMatching(myGame.GetValueOf(5, 4));

            F0 = ImageMatching(myGame.GetValueOf(0, 5));
            F1 = ImageMatching(myGame.GetValueOf(1, 5));
            F2 = ImageMatching(myGame.GetValueOf(2, 5));
            F3 = ImageMatching(myGame.GetValueOf(3, 5));
            F4 = ImageMatching(myGame.GetValueOf(4, 5));
            F5 = ImageMatching(myGame.GetValueOf(5, 5));

            G0 = ImageMatching(myGame.GetValueOf(0, 6));
            G1 = ImageMatching(myGame.GetValueOf(1, 6));
            G2 = ImageMatching(myGame.GetValueOf(2, 6));
            G3 = ImageMatching(myGame.GetValueOf(3, 6));
            G4 = ImageMatching(myGame.GetValueOf(4, 6));
            G5 = ImageMatching(myGame.GetValueOf(5, 6));

        }      

        private void Ending()
        {
            int test = myGame.WinnerPlayer;
            if (test == 1)
            {
                MessageBox.Show("Player One Won !", "End", MessageBoxButton.OK, MessageBoxImage.Information);
                Reset();
            }
            if (test == 2)
            {
                MessageBox.Show("Player Two Won !", "End", MessageBoxButton.OK, MessageBoxImage.Information);
                Reset();
            }
            
        }

        private void Reset()
        {
            ContentVisibility = "Hidden";
        }


        public ICommand AboutCommand
        {
            get
            {
                if (_aboutCommand == null)
                {
                    _aboutCommand = new RelayCommand<object>(param => this.AboutCommandExecute(), param => this.AboutCommandCanExecute);
                }
                return _aboutCommand;
            }
        }

        void AboutCommandExecute()
        {
            MessageBox.Show("This Connect4 was created by Corky Maigre in\nISIMs Engineering School."
                + "\nVisit www.corkymaigre.be", "About", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        bool AboutCommandCanExecute
        {
            get { return true; }
        }


        public ICommand HelpCommand
        {
            get
            {
                if (_helpCommand == null)
                {
                    _helpCommand = new RelayCommand<object>(param => this.HelpCommandExecute(), param => this.HelpCommandCanExecute);
                }
                return _helpCommand;
            }
        }

        void HelpCommandExecute()
        {
            MessageBox.Show("...", "Help", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        bool HelpCommandCanExecute
        {
            get { return true; }
        }


    }
}



